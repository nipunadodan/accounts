jQuery.fn.extend({
    form: function(form, callback) {
        $(this).closest('.modal').find('#form-modal-title').html(form.title);
        $(this).find('form.modal-form').attr(form.formAttrs);

        let str = '<div class="row">';
        $.each(form.inputs,function(key,val) {
            str += '<div class="'+val.col+' text-left">';
            if(val.element === 'input' && val.type !== 'radio' && val.type !== 'checkbox') {
                if(val.label !== null)
                    str += '<label for="' + val.id + '">' + val.label + '</label>';
                str += '<input ' +
                    'class="' + val.class + '" ' +
                    'name="'+val.name+'" ' +
                    'type="'+val.type+'" ' +
                    'value="'+val.value+'" ' +
                    ''+(val.readonly === true ? 'readonly' : '')+' ' +
                    'placeholder="'+val.label+'"' +
                    'id="'+val.id+'">';
            }else if(val.element === 'input' && (val.type === 'radio' || val.type === 'checkbox')){
                str += '<label class="d-block" for="' + val.id + '">' + val.label + '</label>';
                $.each(val.options, function (key, option) {
                    str += '<input type="'+val.type+'" name="'+val.name+'" id="'+option.id+'" class="normal-check" value="'+option.val+'" '+(option.checked === true ? 'checked' : option.checked)+'>';
                    str += '<label for="'+option.id+'" class="mt-0 me-3 cursor-pointer">'+option.text+'</label>';
                });
            }else if(val.element === 'select'){
                str += '<label id="' + val.id + '">' + val.label + '</label>';
                str += '<select ' +
                    'class="' + val.class + '" ' +
                    'name="'+val.name+'" ' +
                    'id="'+val.id+'">';
                $.each(val.options, function (key, option){
                    str += '<option value="'+option.val+'">'+option.text+'</option>';
                });
                str += '</select>';
            }else if(val.element === 'textarea'){
                str += '<label for="' + val.id + '">' + val.label + '</label>';
                str += '<textarea '+
                    'class="' + val.class + '" ' +
                    'name="'+val.name+'" ' +
                    ''+(val.readonly === true ? 'readonly' : '')+' ' +
                    'placeholder="'+val.label+'"' +
                    'id="'+val.id+'"' +
                    '>'+val.value+'</textarea>';
            }
            str += '</div>';
        });
        str += '</div>';

        if (typeof callback == 'function') {
            callback.call(this); // brings the scope to the callback
        }

        return this.find('.modal-body').html(str);
    }
});
//var dyn_functions = [];

dyn_functions['getLast5Transactions'] = function (json, form='') {
    //dyn_functions['o1'](json, form);
    $.ajax({
        url: site_url+'ajax.php?process=transactions-list-process',
        success: function(response) {
            $('#tr-list').html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //console.log('AJAX call failed.');
            //console.log(textStatus + ': ' + errorThrown);
        },
        complete: function() {
            //console.log('AJAX call completed');
        }
    });
}

dyn_functions['transaction-edit'] = function (json){
    $('.modal').modal('hide');
    responseModal(json.status, json.message);
    if(json.status == 'success'){
        dyn_functions['getLast5Transactions'](json, '');
    }
}