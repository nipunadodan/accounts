<nav id="main-nav">
    <ul>
        <li><a href="<?php echo SITE_URL?>">Home</a></li>
        <li><a href="<?php echo SITE_URL?>daily-add">Add Daily</a></li>
        <li>
            <a href="<?php echo SITE_URL?>report">Report</a>
            <ul class="sub-menu">
                <li><a href="<?php echo SITE_URL?>report">Full Report</a></li>
                <li><a href="<?php echo SITE_URL?>report-daily">Daily</a></li>
                <li><a href="<?php echo SITE_URL?>report-monthly">Monthly</a></li>
                <li><a href="">Yearly</a></li>
            </ul>
        </li>
        <li><a href="#">Fixed</a></li>
        <li><a href="<?php echo SITE_URL?>users">Users</a></li>
        <li><a href="<?php echo SITE_URL?>logout">Logout</a></li>
    </ul>
</nav>
