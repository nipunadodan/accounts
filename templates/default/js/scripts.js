$(document).ready(function () {
    $('#more').click(function () {
        $('#more-content').slideToggle('fast');
        $('#more-icon').toggleClass('la-plus la-minus');
    });

    /* menu */
    $('#main-nav').hide();
    $('#menu-button').click(function () {
        $('#main-nav').slideToggle();
        $('section.content').fadeToggle();
        $('#menu-button i').toggleClass('la-bars la-times');
    });

    $('ul.sub-menu').hide();
    $('ul.sub-menu').prev().click(function (e) {
        e.preventDefault();
        $(this).next().slideToggle();
    });
});

function responseModal(status, message){
    $('#response-modal .modal-content').attr('class','modal-content border-0 bg-'+status);
    $('#response-modal #response-modal-title').html(status);
    $('#response-modal #response-modal-icon').attr('class', icons[status]);
    $('#response-modal .modal-body').html("<span class=''>"+message.replace(/!/g, "")+"</span>");
    $('#response-modal').modal('toggle');
    if(debug === true)
        console.log(icons[status]);
}