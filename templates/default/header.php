<!doctype html>
<html><head>
    <link rel="icon" type="image/png" href="https://nipunadodan.com/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Nipuna Dodantenna">
    <meta name="description" content="Experienced Fullstack Web and UX developer who is also a travel buff with a high enthusiasm in travel and landscape photography">
    <meta name="keywords" content="web, ui, ux, developer, php, HTML, HTML5, CSS, CSS3, jQuery, MySQL, UI, UX, design">
    <meta name="theme-color" content="#17a2b8" />

    <title><?php echo SITE_NAME ?></title>

    <!--<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500|Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,900&display=swap" rel="stylesheet">
    <link href="<?php echo CORE_INC_URL ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo TEMPLATE_URL ?>css/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css">

    <script>var site_url = '<?php echo SITE_URL ?>'</script>
    <script>var debug = true</script>
    <script src="<?php echo CORE_INC_URL?>js/jquery.min.js"></script>
    <script src="<?php echo CORE_INC_URL?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo TEMPLATE_URL?>js/scripts.js"></script>

    <script src="<?php echo CORE_INC_URL?>js/scripts.js"></script>
    <script src="<?php echo CORE_INC_URL?>js/ajax.js"></script>
    <script src="<?php echo INC_URL?>js/scripts.js"></script>
</head>
<body>
    <header class="bg-info text-white py-3 sticky-top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="float-left">
                        <a class="text-white " href="<?php echo SITE_URL?>">
                            <i class="las la-wallet la-2x"></i> <span class="h3">eWallet</span>
                        </a>
                    </div>
                    <div class="float-right" id="main_nav">
                        <div id="menu-button" class="cursor-pointer">
                            <i class="la la-bars la-2x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php
include_once TEMPLATE_PATH.'menu.php';
?>
<section class="content">
