<?php
/**
 * Created by PhpStorm.
 * User: nipuna
 * Date: 07/07/19
 * Time: 12:44
 */

class Fd extends \Db\Db{
    private $table = 'fds';

    public function addFd($post){
        $data = self::insert($this->table, [
            'depositor' => $post['depositor'],
            'bank_id' => $post['bank_id'],
            'branch_id' => $post['branch_id'],
            'dep_date' => $post['dep_date'],
            'dep_amount' => $post['dep_amount'],
            'int_months' => $_POST['int_months'],
            'interest' => $_POST['interest'],
            'dep_months' => $_POST['dep_months'],
            'dep_months' => $_POST['dep_months'],
            'mat_amount' => $_POST['mat_amount'],
            'mat_amount' => $_POST['mat_amount'],
            'receipt_no' => $_POST['receipt_no'],
            'receipt_no' => $_POST['receipt_no'],
            'renewed_from' => $_POST['renewed_from'],
            'renewed_to' => $_POST['renewed_to'],
            'tax_percent' => $_POST['tax_percent'],
            'nominee' => $_POST['nominee'],
            'status' => $_POST['status'],
            'updated_by' => $_POST['updated_by']
        ]);

        return $data->rowCount();
    }
}