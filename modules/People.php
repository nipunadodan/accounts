<?php
/**
 * Created by PhpStorm.
 * User: nipuna
 * Date: 07/07/19
 * Time: 12:11
 */

class People extends \User\User{
    function usersList (){
        return self::query("SELECT * FROM ".DB_PREFIX."users ORDER BY first_name")->fetchAll(PDO::FETCH_OBJ);
    }
}