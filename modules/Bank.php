<?php
/**
 * Created by PhpStorm.
 * User: nipuna
 * Date: 07/07/19
 * Time: 12:17
 */

class Bank extends \Db\Db {
    public function getAllBanks(){
        return self::query('SELECT * FROM ".DB_PREFIX."banks')->fetchAll(PDO::FETCH_OBJ);
    }

    public function getAllBranches(){
        return self::query('SELECT * FROM ".DB_PREFIX."branches WHERE bank_id =".$bank_id." ORDER BY branch_name')->fetchAll(PDO::FETCH_OBJ);
    }
}