<?php
/**
 * Created by PhpStorm.
 * User: nipuna
 * Date: 24/06/19
 * Time: 00:57
 */

use Db\Db;

class Accounts extends Db{
    private $table = 'daily_accounts';
    private $sources = 'sources';
    private $trans_types = 'trans_types';
    private $id;
    private $record_date;
    private $description;
    private $amount;
    private $note; // aka source
    protected $type; // 1 - income | 2 - expense
    private $status;
    private $user_id;

    /**
     * @return mixed
     */
    public function getSources(){
        return self::select($this->sources,'*');
    }

    public function getTypes(){
        return self::select($this->trans_types,'*');
    }

    public function createTransaction($post){
        $data = self::insert($this->table, [
            'record_date' => $post['date'],
            'description' => $post['desc'],
            'amount' => $post['amount'],
            'source' => $post['source'],
            'type' => $post['type'],
            'user_id' => 1 //$_SESSION['user_id']
        ]);
        return $data->rowCount();
    }

    public function getAllTransactions($limit=null){
        return self::select($this->table, [
            '[>]trans_types' => ['type' => 'id']
        ],[
            'daily_accounts.id',
            'daily_accounts.record_date',
            'daily_accounts.description',
            'daily_accounts.amount',
            'daily_accounts.source',
            'daily_accounts.type',
            'daily_accounts.status',
            'daily_accounts.user_id',
            'daily_accounts.created_at',
            'daily_accounts.updated_at',
            'trans_types.id(trans_id)',
            'trans_types.trans_name',
            'trans_types.in_out',
            'trans_types.real_trans',
        ],[
            //'daily_accounts.type' => 5,
            'ORDER' => [
                'record_date' => 'DESC',
                'created_at' => 'DESC'
            ],
            'LIMIT' => $limit
        ]);
    }

    public function editTransaction($transID){
        $update = self::update($this->table,[
            'record_date' => $_POST['transaction-date'],
            'description' => $_POST['transaction-description'],
            'amount' => $_POST['transaction-amount'],
            'source' => $_POST['transaction-source'],
            'type' => $_POST['transaction-type'],
            'user_id' => 1 //$_SESSION['user_id']
        ],[
            'id' => $transID
        ]);

        return $update->rowCount();
    }

    public function getDailyReport(){
        return self::query('select cast(`'.DB.'`.`acc_daily_accounts`.`record_date` as date) AS `date`,sum(coalesce((case when (`'.DB.'`.`acc_daily_accounts`.`type` = 1) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `income`,sum(coalesce((case when (`'.DB.'`.`acc_daily_accounts`.`type` = 2) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `expenditure` from `'.DB.'`.`acc_daily_accounts` group by cast(`'.DB.'`.`acc_daily_accounts`.`record_date` as date) order by cast(`'.DB.'`.`acc_daily_accounts`.`record_date` as date) desc')->fetchAll();
    }

    public function getTransaction(){

    }

    public function updateTransaction(){

    }

    public function getThisMonthIncome(){
        return self::query('SELECT SUM(amount) AS "income" FROM `acc_daily_accounts` AS A LEFT JOIN `acc_trans_types` B ON A.`type` = B.`id` WHERE B.`in_out` = 1 AND B.`real_trans` =1 AND YEAR(A.`record_date`) = YEAR(CURDATE()) AND MONTH(A.`record_date`) = MONTH(CURDATE())')->fetchAll();
    }


    public function getThisMonthExpenditure(){
        return self::query('SELECT SUM(amount) AS "expense" FROM `acc_daily_accounts` AS A LEFT JOIN `acc_trans_types` B ON A.`type` = B.`id` LEFT JOIN `acc_sources` C ON A.`source` = C.`id` WHERE B.`in_out` = 0 AND B.`real_trans` = 1 AND YEAR(A.`record_date`) = YEAR(CURDATE()) AND MONTH(A.`record_date`) = MONTH(CURDATE()) AND (A.`type` <> 0 AND C.`type` <> 0)')->fetchAll();
    }

    public function getSourceBalance($source){

    }

    public function getOverallBalance(){
        return self::query('SELECT SUM(COALESCE(CASE WHEN B.`in_out` = 1 AND C.`type` = 1 THEN `amount` END, 0)) - SUM(COALESCE(CASE WHEN B.`in_out` = 0 AND C.`type` = 1 THEN `amount` END, 0)) AS "balance" FROM `acc_daily_accounts` AS A LEFT JOIN `acc_trans_types` B ON A.`type` = B.`id` LEFT JOIN `acc_sources` AS C ON A.`source` = C.`id`')->fetchAll();
    }

    public function getMonthlyReport(){
        return self::query('select concat(year(`'.DB.'`.`acc_daily_accounts`.`record_date`),\'-\',month(`'.DB.'`.`acc_daily_accounts`.`record_date`)) AS `date`,
       sum(coalesce((case when (`'.DB.'`.`acc_daily_accounts`.`type` =1 and `'.DB.'`.`acc_sources`.`type` = 1 and `'.DB.'`.`acc_trans_types`.`real_trans` = 1) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `income`, 
       sum(coalesce((case when (`'.DB.'`.`acc_daily_accounts`.`type` = 2 and `'.DB.'`.`acc_sources`.`type` = 1 and `'.DB.'`.`acc_trans_types`.`real_trans` = 1) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) - sum(coalesce((case when (`'.DB.'`.`acc_daily_accounts`.`type` = 5) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `expenditure` 
from `'.DB.'`.`acc_daily_accounts` 
  left join `acc_trans_types` on `acc_daily_accounts`.`type` = `acc_trans_types`.`id` 
  left join `'.DB.'`.`acc_sources` ON `'.DB.'`.`acc_daily_accounts`.`source` = `'.DB.'`.`acc_sources`.`id`
 
group by month(`'.DB.'`.`acc_daily_accounts`.`record_date`),year(`'.DB.'`.`acc_daily_accounts`.`record_date`) 
order by cast(`'.DB.'`.`acc_daily_accounts`.`record_date` as date) desc')->fetchAll();
    }

    public function monthlyIncomeReport(){
        return self::query('select concat(year(`'.DB.'`.`acc_daily_accounts`.`record_date`),\'-\',month(`'.DB.'`.`acc_daily_accounts`.`record_date`)) AS `date`,
        sum(coalesce(case when (`'.DB.'`.`acc_trans_types`.`id` = 1) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `income`,
        sum(coalesce(case when (`'.DB.'`.`acc_trans_types`.`id` = 2) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `expenditure`,
        sum(coalesce(case when (`'.DB.'`.`acc_trans_types`.`id` = 3) then `'.DB.'`.`acc_daily_accounts`.`amount` end),0)) AS `withdra`,
        ');
    }

    public function getTodayExpenses(){

    }

    public function getTodayIncomes(){

    }
}
