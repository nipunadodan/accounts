<?php
session_start();
include_once 'config.php';
require 'vendor/autoload.php';
$current_user = new \User\User();
//include_once CORE_PATH.'includes/db_conn.php';
include_once CORE_PATH.'index.php';
include_once INC_PATH.'functions.php';

include_once TEMPLATE_PATH.'header.php';
include_once TEMPLATE_PATH.'content.php';
include_once TEMPLATE_PATH.'footer.php';

