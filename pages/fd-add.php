<?php
$banks = new Bank();
?>
<div class="container">
    <h1 class="text-thin text-center py-4">Add new FD</h1>
    <nav class="page-menu">
        <ul>
            <li>MENU: </li>
            <li><a href="<?php echo SITE_URL.'fd-list/'?>">All FDs <i class="pe-7s-cash pe-2x pe-fw pe-va"></i></a></li>
        </ul>
    </nav>
    <div class="row justify-content-center">
        <div class="col-md-8" id="add-fd-form">
            <form class="o1 form" name="add-fd" action="" method="post" data-reset="yes" enctype="application/x-www-form-urlencoded">
                <div class="row">
                    <div class="col-md-6">
                        <label for="dep_date">Deposit Date*</label>
                        <input name="dep_date" type="date" id="dep_date" required />
                    </div>
                    <div class="col-md-6">
                        <label for="bank_name">Depositor</label>
                        <select name="depositor" required>
                            <?php
                            $people = new People();
                            $users = $people->usersList();
                            foreach($users as $user){
                                echo '<option value="'.$user->id.'">'.$user->first_name.' '.$user->last_name."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="bank_id">Bank Name</label>
                        <select name="bank_id" id="bank-select">
                            <option value="0">Select bank name</option>
                            <?php
                            $list = $banks->getAllBanks();
                            //pre($list);
                            foreach ($list as $bank){
                                echo '<option value="'.$bank->id.'">'.$bank->bank_name.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="branch">Branch Name</label>
                        <select name="branch_id" id="branch">
                            <option value="0">Select branch name</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="dep_amount">Deposit amount*</label>
                        <input name="dep_amount" id="dep_amount" type="number" step="0.01" placeholder="Deposit amount" required />
                    </div>
                    <div class="col-md-2">
                        <label for="interest">Interest</label>
                        <input name="interest" id="interest" type="number" step="0.01" placeholder="Interest">
                    </div>
                    <div class="col-md-3">
                        <label for="dep_months">Deposit months</label>
                        <input name="dep_months" id="dep_months" type="number" step="0.01" placeholder="Deposit months">
                    </div>
                    <div class="col-md-3">
                        <label for="int_months">Interest months</label>
                        <input name="int_months" id="int_months" type="number" step="0.01" value="12" placeholder="Interest months">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="mat_amount">Maturity amount</label>
                        <input name="mat_amount" id="mat_amount" type="number" step="0.01" placeholder="Maturity amount">
                    </div>
                    <div class="col-md-6">
                        <label for="receipt_no">Receipt Number</label>
                        <input name="receipt_no" id="receipt_no" type="text" placeholder="Receipt Number">
                    </div>
                    <div class="col-md-2">
                        <label for="tax_percent">Tax (%)</label>
                        <input name="tax_percent" id="tax_percent" type="number" step="0.01" placeholder="Tax (%)">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="notes">Nominee</label>
                        <select name="nominee">
                            <option value="0">No nominee</option>
                            <?php
                            $users = $people->usersList();
                            foreach($users as $user){
                                echo '<option value="'.$user->id.'">'.$user->first_name.' '.$user->last_name."</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="notes">Notes</label>
                        <input name="notes" id="notes" type="text" placeholder="Notes">
                    </div>
                </div>

                <input type="hidden" name="updated_by" value="<?php $_SESSION['user_id']?>" />

                <div class="row my-5">
                    <div class="col-md-4 col-6 ">
                        <input type="reset" class="cursor-pointer" />
                    </div>
                    <div class="col-md-4 col-6">
                        <input class="bg-success cursor-pointer" type="submit" value="ADD" />
                    </div>
                </div>
                <div class="message"></div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#mat_amount').focus (function () {
        var mat_amount = parseFloat($('#dep_amount').val())*(parseFloat($('#interest').val())+100)/100*parseFloat($('#dep_months').val())/parseFloat($('#int_months').val());
        console.log(mat_amount);

        $('#mat_amount').val(mat_amount.toFixed(2));
    });
</script>