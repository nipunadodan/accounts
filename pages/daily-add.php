<?php
//use Accounts;
$sources = new Accounts();
$sourcesList = $sources->getSources();
$typesList = $sources->getTypes();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-thin text-center pt-4 pb-3">Add New Transaction</h1>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form class="form pb-5 ajax" name="daily-add" method="post" data-reset="yes" data-func="getLast5Transactions" data-popup="yes" data-responsemodal="yes">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="type">Type</label>
                                <div class="icon-radio-buttons scroll-x">
                                    <div class="">
                                    <?php
                                        foreach ($typesList as $type){
                                            $checked = $type['id'] == 2 ? 'checked' : '';
                                            echo '
                                            <input type="radio" name="type" value="'.$type['id'].'" id="type_'.$type['id'].'" '.$checked.'><label for="type_'.$type['id'].'" class="m-0 px-4" title="'.$type['trans_name'].'">'.$type['trans_name'].'</label>
                                        ';
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="source">Source (In/Out)</label>
                                <div class="icon-radio-buttons scroll-x">
                                    <div class="">
                                        <?php
                                        $i=0;
                                        foreach ($sourcesList as $source){
                                            $checked = ($i == 1) ? 'checked' : '';
                                            $i++;
                                            ?>
                                            <input type="radio" name="source" value="<?php echo $source['id']?>" id="<?php echo 'source_'.$source['id']?>" <?php echo $checked; ?>><label for="<?php echo 'source_'.$source['id']?>" class="m-0 px-4"><?php echo $source['source']?></label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mt-3">
                                <label for="date">Date</label>
                                <input type="date" name="date" id="date" value="<?php echo date('Y-m-d')?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 mt-3">
                                <label for="desc">Description</label>
                                <input type="text" name="desc" id="desc" placeholder="Description" />
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="amount">Amount</label>
                                <input type="number" name="amount" id="amount" step="0.01" placeholder="Amount" />
                            </div>
                        </div>


                        <div class="row mt-5">
                            <div class="col-md-4 col-6 ">
                                <input type="reset" class="cursor-pointer" />
                            </div>
                            <div class="col-md-4 col-6">
                                <input class="bg-success cursor-pointer" type="submit" value="ADD" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$accoountsCls = new Accounts();
$data = $accoountsCls->getAllTransactions(15);
//should be fine
?>
    <section class="py-3">
        <div class="container" id="tr-list">
            <?php
            foreach ($data as $fd) {
                ?>
                <div class="row border-bottom py-3">
                    <div class="col-md-6 col-5">
                        <div class="row">
                            <div class="col-md-6"><?php echo $fd['description']; ?></div>
                            <div class="col-md-6 text-md-center text-muted text-small"><?php echo $fd['record_date'].' '.date("D", strtotime($fd['record_date'])); ?></div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="row">
                            <div class="col-md-6 text-right">LKR <?php echo number_format($fd['amount'],2); ?></div>
                            <div class="col-md-6 text-md-center text-right text-small">
                                <?php echo $fd['in_out']== 0 ? '<span class
="text-danger">'.$fd['trans_name'].'</span>' : ($fd['in_out'] == 1 ? '<span class="text-success">'.$fd['trans_name'].'</span>' : ''); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-2">
                        <span class="btn btn-sm btn-info tr-edit" data-transaction='<?php echo json_encode($fd);?>' id="<?php echo $fd['id'];?>">
                            <i class="la la-pencil"></i>
                        </span>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>

<script>
    $('#tr-list').on('click', '.tr-edit', function (e) {
        e.preventDefault();
        const transaction = $(this).data('transaction');
        const form = {
            title:'Edit Transaction',
            formAttrs: {
                'id': 'transaction-edit',
                'name': 'transaction-edit',
                'method': 'post',
                'data-func': 'transaction-edit',
                'data-validation': 'yes'
            },
            inputs: [
                {
                    element:'input',
                    type:'hidden',
                    value:transaction.id,
                    name:'transactionID',
                    label:null
                },
                {
                    label:'Transaction Type',
                    name:'transaction-type',
                    element:'input',
                    type:'radio',
                    id: 'transaction-type',
                    options:[
                        <?php
                        foreach($typesList as $type){
                            echo "{id:'transID_".$type['id']."', text:'".$type['trans_name']."',val:'".$type['id']."',checked:(transaction.type == ".$type['id']." ? true : false)},";
                        }
                        ?>
                    ],
                    readonly: false,
                    col:'col-6',
                    class:'',
                },
                {
                    label:'Source',
                    name:'transaction-source',
                    element:'input',
                    type:'radio',
                    id: 'transaction-source',
                    options:[
                        <?php
                        foreach($sourcesList as $source){
                            echo "{text:'".$source['source']."',val:'".$source['id']."', id:'sourceID_".$source['id']."',checked:(transaction.source == ".$source['id']." ? true : false)},";
                        }
                        ?>
                    ],
                    readonly: false,
                    col:'col-6',
                    class:'',
                },
                {
                    label:'Date',
                    name:'transaction-date',
                    element:'input',
                    type:'date',
                    id: 'transaction-date',
                    value: transaction.record_date,
                    readonly: false,
                    col:'col-md-6',
                    class:'',
                },
                {
                    label:'Amount',
                    name:'transaction-amount',
                    element:'input',
                    type:'number',
                    id: 'transaction-amount',
                    value: transaction.amount,
                    readonly: false,
                    col:'col-md-6',
                    class:'',
                },
                {
                    label:'Description',
                    name:'transaction-description',
                    element:'textarea',
                    type:'',
                    id: 'transaction-description',
                    value: transaction.description,
                    readonly: false,
                    col:'col-md-12',
                    class:'',
                },/*
                {
                    label:'Category',
                    name:'transaction-category',
                    element:'input',
                    type:'text',
                    id: 'transaction-category',
                    value: '',
                    readonly: false,
                    col:'col-md-12',
                    class:'',
                },*/
            ]};
        $('#formModal').form(form, function () {
            $('#formModal').modal('show');
        });

    });
</script>
