<?php
$accoountsCls = new Accounts();
$allIncomeMonth = $accoountsCls->getThisMonthIncome();
$allExpenditureMonth = $accoountsCls->getThisMonthExpenditure();
$overallBalance = $accoountsCls->getOverallBalance();

?>
<div class="summary-header bg-info pt-0 pb-4 text-white">
    <div class="container">
        <div class="row py-3">
            <div class="col-12">
                <h2 class="text-center text-thin display-4 my-0"><span class="h6">LKR</span> <?php echo empty($overallBalance) ? 0 : number_format($overallBalance[0]['balance'],2)?></h2>
                <p class="text-center text-small m-0">Balance</p>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <h2 class="text-left text-thin display-6 my-0">
                    <?php echo empty($allIncomeMonth) ? 0 : number_format($allIncomeMonth[0]['income'],2); ?>
                </h2>
                <p class="text-small text-left p-0 m-0">Income (<?php echo date('M Y') ?>)</p>
            </div>
            <div class="col-6">
                <h2 class="text-right text-thin display-6 my-0">
                    <?php echo empty($allExpenditureMonth) ? 0 : number_format($allExpenditureMonth[0]['expense'],2); ?>
                </h2>
                <p class="text-small text-right p-0 m-0">Expenses (<?php echo date('M Y') ?>)</p>
            </div>
        </div>
    </div>
</div>
<?php
$data = $accoountsCls->getAllTransactions();
//pre($data);
?>
<section class="py-3">
    <div class="container">
        <?php
        foreach ($data as $fd) {
            ?>
            <div class="row border-bottom py-3">
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6"><?php echo $fd['description']; ?></div>
                        <div class="col-md-6 text-md-center text-muted text-small"><?php echo $fd['record_date']; ?></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-md-6 text-right">LKR <?php echo number_format($fd['amount'],2); ?></div>
                        <div class="col-md-6 text-md-center text-right text-small">
                            <?php echo $fd['in_out']== 0 ? '<span class
="text-danger">'.$fd['trans_name'].'</span>' : ($fd['in_out'] == 1 ? '<span class="text-success">'.$fd['trans_name'].'</span>' : ''); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>

