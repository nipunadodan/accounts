<div class="container">
    <div class="row align-content-center text-center" style="min-height: calc(100vh - 66px)">
        <div class="col-12">
            <h1 class="text-thin text-loose">Welcome</h1>
            <hr>
            <h2 class="h6 text-muted text-thin"><?php echo SITE_TAGLINE; ?></h2>
        </div>
    </div>
</div>
