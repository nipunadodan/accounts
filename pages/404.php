<div class="row">
    <div class="col-md-12">
        <div class="row align-items-center">
            <div class="col py-5 px-4 rounded-right bg-white text-center">
                <h2 class="text-bold text-muted" style="font-size: 9rem;">404</h2>
                <h1 class="h2 text-muted">Oops! Sorry, we could not find this page.</h1>
            </div>
        </div>
    </div>
</div>
