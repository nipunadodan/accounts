-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 19, 2019 at 10:01 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.3.7-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orange_financial`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_sources`
--

CREATE TABLE `acc_sources` (
  `id` int(11) NOT NULL,
  `source` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_sources`
--

INSERT INTO `acc_sources` (`id`, `source`, `description`, `updated_at`) VALUES
(1, 'Cash', 'Cash', '2019-06-26 04:24:21'),
(2, 'NSB', '101020278742', '2019-06-26 04:22:52'),
(3, 'BOC CC', '****7553', '2019-06-26 04:22:52');

-- --------------------------------------------------------

--
-- Table structure for table `acc_trans_types`
--

CREATE TABLE `acc_trans_types` (
  `id` int(11) NOT NULL,
  `trans_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `in_out` tinyint(1) NOT NULL,
  `real_trans` tinyint(1) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_trans_types`
--

INSERT INTO `acc_trans_types` (`id`, `trans_name`, `in_out`, `real_trans`, `updated`) VALUES
(1, 'Income', 1, 1, '2019-07-04 05:41:57'),
(2, 'Expenditure', 0, 1, '2019-07-04 05:41:33'),
(3, 'Withdrawal', 0, 0, '2019-07-04 06:30:41'),
(4, 'For FD', 0, 0, '2019-07-04 06:30:53'),
(5, 'Reimbursement', 1, 0, '2019-07-04 06:31:05'),
(6, 'Refund', 1, 0, '2019-07-04 06:31:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_sources`
--
ALTER TABLE `acc_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acc_trans_types`
--
ALTER TABLE `acc_trans_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_sources`
--
ALTER TABLE `acc_sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `acc_trans_types`
--
ALTER TABLE `acc_trans_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
