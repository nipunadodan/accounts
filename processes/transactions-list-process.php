<?php
    $accoountsCls = new Accounts();
    $data = $accoountsCls->getAllTransactions(15);
    //pre($data);

    foreach ($data as $fd) {
?>
    <div class="row border-bottom py-3">
        <div class="col-md-6 col-5">
            <div class="row">
                <div class="col-md-6"><?php echo $fd['description']; ?></div>
                <div class="col-md-6 text-md-center text-muted text-small"><?php echo $fd['record_date'].' '.date("D", strtotime($fd['record_date'])); ?></div>
            </div>
        </div>
        <div class="col-5">
            <div class="row">
                <div class="col-md-6 text-right">LKR <?php echo number_format($fd['amount'],2); ?></div>
                <div class="col-md-6 text-md-center text-right text-small">
                    <?php echo $fd['in_out']== 0 ? '<span class
="text-danger">'.$fd['trans_name'].'</span>' : ($fd['in_out'] == 1 ? '<span class="text-success">'.$fd['trans_name'].'</span>' : ''); ?>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-2">
            <span class="btn btn-sm btn-info tr-edit" data-transaction='<?php echo json_encode($fd);?>' id="<?php echo $fd['id'];?>">
                <i class="la la-pencil"></i>
            </span>
        </div>
    </div>
<?php } ?>