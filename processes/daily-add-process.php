<?php

$accounts = new Accounts();

if(isset($_POST['date'], $_POST['type'], $_POST['desc'], $_POST['amount'], $_POST['source']) && $_POST['date'] !== '' && $_POST['type'] !== '' && $_POST['desc'] !== '' && $_POST['amount'] !== '' && $_POST['source'] !== ''){

    if($accounts->createTransaction($_POST) > 0){
        echo json_encode(array(
            'message' => 'Successfully created a record',
            'status' => 'success'
        ));
    }else{
        echo json_encode(array(
            'message' => 'Unexpected error occurred',
            'status' => 'danger'
        ));
    }
}else{
    echo json_encode(array(
        'message' => 'Required field/s missing',
        'status' => 'warning'
    ));
}
