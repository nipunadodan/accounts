<?php
if(isset($_POST['transactionID']) && $_POST['transactionID'] !==''){
    $update = (new Accounts())->editTransaction($_POST['transactionID']);

    if($update > 0){
        $return = [
            'status' => 'success',
            'message' => 'Successfully updated'
        ];
    }else{
        $return = [
            'status' => 'danger',
            'message' => 'Update failed'
        ];
    }
}else{
    $return = [
        'status' => 'danger',
        'message' => 'Insufficient data'
    ];
}

print_r(json_encode($return));