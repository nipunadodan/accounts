<?php
define("SITE_NAME", "Orange");
define("SITE_TAGLINE", "Lightweight PHP Framework");
define("DOMAIN", (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/");
define("SITE_ROOT", "nipunadodan/orange/");

define("DB_HOST", "localhost");
define("DB", "orange");
define("DB_USER", "nipuna");
define("DB_PASSWORD", "nipuna123");
define("DB_PREFIX", "o_");

define('APP_ID', ''); //set your unique app id here

date_default_timezone_set('Asia/Colombo'); //set your timezone here
